# The suckless terminal, st

My fork of the suckless terminal from https://suckless.org.

## Changes

- Patched to enable scrollback: `shift + mousewheel` or `shift + pageUp/pageDown` are the defaults.
- Fits doom colorscheme
